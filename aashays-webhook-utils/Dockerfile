ARG debianrelease=bookworm

FROM rust:${debianrelease} as rustbuild
RUN apt update && apt install -y --no-install-recommends python3 python3-pip python3-venv musl-dev python3-dev libssl-dev libffi-dev
RUN python3 -m venv ~/.local --system-site-packages
RUN ~/.local/bin/pip install pyvizio auri libdyson

FROM golang:${debianrelease} as gobuild
RUN apt update && apt install -y --no-install-recommends git
RUN go install github.com/adnanh/webhook@latest

FROM ${dockerarch}debian:${debianrelease}
COPY --from=gobuild /go/bin/webhook /usr/local/bin/webhook
COPY --from=rustbuild /root/.local /root/.local
RUN apt update && apt install -y --no-install-recommends wget python3 libjpeg-dev libopenjp2-7 libtiff5-dev libxcb1-dev && rm -rf /var/lib/apt/lists/*

ENV PATH=/root/.local/bin:$PATH
ENTRYPOINT ["/usr/local/bin/webhook"]
CMD ["-hooks", "/etc/webhook/hooks.json", "-verbose"]
VOLUME ["/etc/webhook"]
EXPOSE 9000
HEALTHCHECK CMD wget --quiet --tries=1 --spider http://localhost:9000 || exit 1
